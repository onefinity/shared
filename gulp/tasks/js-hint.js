var gulp = require('gulp'),
    cache = require('gulp-cached'),
    jshint = require('gulp-jshint'),
    plumber = require('gulp-plumber'),
    combiner = require('stream-combiner2');
var errorHandler = require('../helpers/errorHandler.js');
var config = require('../config.js').js.hint;

module.exports = {
    func: function() {
        return combiner.obj([
            gulp.src(config.src),
            cache('js-hint'),
            plumber(errorHandler),
            jshint()
        ].concat(config.reporters.map(function(reporter) {
            return jshint.reporter(reporter)
        })));
    }
}