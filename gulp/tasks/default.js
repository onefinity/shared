var gutil = require('gulp-util');
var production = require('../config.js').production;

if ( production ) gutil.log(gutil.colors.blue.bold('Running production tasks, this might take a while...'));

module.exports = {
    // We only want to start our watches during development so let's just
    // execute the watch dependencies when the --production flag was provided
    deps: ( production ) ? require('./watch.js').deps : ['watch'],
    func: function() {
        if ( production ) {
            setTimeout(function() { gutil.log(gutil.colors.blue.bold('Production tasks executed successfully ♥')); }, 0);
        }
    }
}