var _ = require('lodash'),
    gulp = require('gulp'),
    gutil = require('gulp-util');
var config = require('../config.js');

module.exports = {
    deps: config.tasks,
    func: function() {
        // Create watchers as defined in config
        _.each(config.watch, function(watcher) {
            var files = watcher.files,
                tasks = watcher.tasks || [];

            gulp.watch(files, tasks);
        });

        gutil.log(gutil.colors.green.bold('Watching for changes...'));
    }
}