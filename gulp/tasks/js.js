var es = require('event-stream'),
    gulp = require('gulp'),
    path = require('path'),
    gutil = require('gulp-util'),
    gulpif = require('gulp-if'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    extend = require('extend'),
    uglify = require('gulp-uglify'),
    insert = require('gulp-insert'),
    plumber = require('gulp-plumber'),
    watchify = require('watchify'),
    browserify = require('browserify'),
    sourcemaps = require('gulp-sourcemaps');
var errorHandler = require('../helpers/errorHandler.js');
var config = require('../config.js').js,
    production = require('../config.js').production;

module.exports = {
    deps: ['js-hint'],
    func: function() {
        var tasks = config.browserify.bundles.map(function(entry) {
            var opts = extend({}, watchify.args, config.browserify, { entries: [entry] }),
                bundler = ( production ) ? browserify(opts) : watchify(browserify(opts));

            bundler.on('update', rebundle);
            bundler.on('log', gutil.log);

            function rebundle() {
                return bundler.bundle()
                    .on('error', errorHandler)
                    .pipe(plumber(errorHandler))
                    .pipe(source(path.basename(entry)))
                    .pipe(buffer())
                    .pipe(gulpif(production, uglify(config.uglify)))
                    .pipe(insert.wrap(config.prepend || '', config.append || ''))
                    .pipe(gulpif(!production, sourcemaps.init({ loadMaps: true })))
                    .pipe(gulpif(!production, sourcemaps.write('.')))
                    .pipe(gulp.dest(config.dest));
            }

            return rebundle();
        });

        return es.merge.apply(null, tasks);
    }
}
