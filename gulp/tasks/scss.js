var gulp = require('gulp'),
    sass = require('gulp-sass'),
    insert = require('gulp-insert'),
    gulpif = require('gulp-if'),
    plumber = require('gulp-plumber'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer');
var errorHandler = require('../helpers/errorHandler.js');
var config = require('../config.js').scss,
    production = require('../config.js').production;

module.exports = {
    func: function() {
        return gulp.src(config.src)
            .pipe(plumber(errorHandler))
            .pipe(gulpif(!production, sourcemaps.init()))
            // Since there are some problems between gulp-plumber and gulp-sass
            // we'll have to catch scss errors and emit the end event ourselves
            // https://github.com/dlmanning/gulp-sass/issues/90
            .pipe(sass(config.options).on('error', function() { this.emit('end'); }))
            .pipe(autoprefixer(config.autoprefixer))
            .pipe(insert.wrap(config.prepend || '', config.append || ''))
            .pipe(gulpif(!production, sourcemaps.write('.')))
            .pipe(gulp.dest(config.dest));
    }
}
