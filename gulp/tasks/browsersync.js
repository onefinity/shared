var path = require('path'),
    gutil = require('gulp-util'),
    browsersync = require('browser-sync').create();
var config = require('../config.js').browsersync,
    production  = require('../config.js').production;

module.exports = {
    func: function() {
        if ( !production ) {
            gutil.log(gutil.colors.green.bold('Starting Browsersync server...'));

            browsersync.init(config);
            browsersync.watch(path.join(require('../config.js').dest, '**/!(*.map)')).on('change', browsersync.reload);
            browsersync.watch('**/*.{html,php}').on('change', browsersync.reload);
        }
    }
}