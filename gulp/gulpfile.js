var _ = require('lodash'),
    path = require('path'),
    glob = require('glob'),
    gulp = require('gulp');

module.exports = function(config) {
    config = _.merge(require(path.join(__dirname + '/config.js')), config, function(a, b) {
        // Overwrite arrays instead of extending them
        if ( _.isArray(b) ) return b;
    });

    // Create object that will hold all Gulp tasks and get a list of available tasks
    var tasks = {},
        files = glob.sync(path.join(__dirname, '/tasks/**/*.js'));

    // Loop over all files/tasks and make them available to the module
    _.each(files, function(file) {
        var task = require(file),
            deps = task.deps || [], // Dependencies for the Gulp task
            func = task.func, // Callback that contains the gulp task
            name = path.basename(file, path.extname(file)); // Derive name of task from filename

        // Add to tasks object for extensibility
        tasks[name] = func;

        // Create actual gulp task
        gulp.task(name, deps, func);
    });

    return {
        tasks: tasks,
        config: config
    };
}
