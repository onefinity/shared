var _ = require('lodash'),
    gutil = require('gulp-util'),
    notify = require('gulp-notify');

module.exports = function(err) {
    var args = Array.prototype.slice.call(arguments);
    notify.logLevel(0);

    if ( typeof err === 'string' ) { // Handle gulp .on('error', ...) errors
        notify.onError({
            title: 'Error',
            message: err
        }).apply(this, args);

        gutil.log(gutil.colors.red('The following error occurred:'));
        console.log('\n%s\n\n', gutil.colors.gray(err));

        this.emit('end');
    } else if ( typeof err !== 'undefined') {
        var plugin = err.plugin,
            type = err.name || 'Error',
            message = err.formatted || err.message;

        var notification = {
            title: 'Error',
            message: message
        };

        if ( typeof type !== 'undefined' ) notification.title = _.capitalize(type);
        if ( typeof plugin !== 'undefined' ) notification.title += ' in ' + plugin;

        // Show OS specific notification
        notify.onError(notification).apply(this, args);

        // Log to console
        gutil.log(gutil.colors.red('The following error occurred%s:'), ( typeof plugin !== 'undefined' ) ? ' in ' + gutil.colors.bold(plugin) : '');
        console.log('\n%s\n\n', gutil.colors.gray(err.formattedMessage || message));

        this.emit('end');
    } else { // Handle weird errors
        notify.onError({
            title: 'Unrecognized error',
            message: 'An error has occurred, no details have been provided...'
        }).apply(this, args);

        gutil.log(gutil.colors.red('An unrecognized error has occurred, no details have been provided...'));
    }
}